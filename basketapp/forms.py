from django.forms import ModelForm, Select, ChoiceField
from basketapp.models import BasketItem

class AddProductForm(ModelForm):
	class Meta:
		model = BasketItem
		fields = ['quantity']

	def __init__(self, *args, **kwargs):
	        super().__init__(*args, **kwargs)
	        for field_name, field in self.fields.items():
	        	print(field.widget.attrs)
	        	if field_name == 'quantity':
	        		field.widget.attrs.update({'class':'quantity'})
	        		print(field.widget.attrs)
