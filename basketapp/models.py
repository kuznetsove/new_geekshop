from django.db import models
from django.conf import settings
from shopapp.models import Product
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class BasketManager(models.Manager):

	def get_active(self, user):
		return self.filter(status=True, user=user)

	def get_order(self, user):
		return self.get_active(user=user)[0]


class BasketItemManager(models.Manager):

	def orders_list(self, order):
		return self.filter(order_id=order).values_list('product', flat=True)


class Basket(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	order_date = models.DateField(verbose_name=_('Date'), auto_now_add=True)
	status = models.BooleanField('open', default=True)

	objects = BasketManager()


class BasketItem(models.Model):
	order_id = models.ForeignKey(Basket, on_delete=models.CASCADE)
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	quantity = models.PositiveIntegerField(verbose_name=_('quantity'), default=1)
	add_datetime = models.DateTimeField(verbose_name=_('time'), auto_now_add=True)

	objects = BasketItemManager()




