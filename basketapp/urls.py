from django.urls import path
from basketapp.views import OrdersView, BasketView, RemoveFromBasketView, DeleteBasketView
import os

app_name = 'basketapp'

urlpatterns = [
	path('orders/', OrdersView.as_view(), name='orders'),
	path('orders/<int:pk>/', BasketView.as_view(), name='basket'),
	path('remove/<int:pk>/', RemoveFromBasketView.as_view(), name='remove'),
	path('delete/<int:pk>/', DeleteBasketView.as_view(), name='delete'),
]

