from django.contrib import admin
from basketapp.models import Basket, BasketItem

# Register your models here.

class BasketAdmin(admin.ModelAdmin):
	list_display = ['id', 'order_date']

admin.site.register(Basket, BasketAdmin)

class BasketItemAdmin(admin.ModelAdmin):
	list_display = ['order_id', 'product', 'quantity', 'add_datetime']

admin.site.register(BasketItem, BasketItemAdmin)
