from django import template

register = template.Library()

# @register.filter(name='to_num')
def dec_to_num(dec):
	num = int(dec)
	return num

register.filter('to_num', dec_to_num)