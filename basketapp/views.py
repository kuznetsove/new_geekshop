from django.http import HttpResponseRedirect
from basketapp.models import Basket, BasketItem
from django.forms import Form
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from basketapp.forms import AddProductForm
from django.urls import reverse_lazy

# Create your views here.

class OrdersView(LoginRequiredMixin, ListView):
	'''Represent list of active orders'''
	login_url = reverse_lazy('authapp:login')
	model = Basket

	def get_context_data(self, **kwargs):
		context = super(OrdersView, self).get_context_data(**kwargs)
		context['orders'] = Basket.objects.filter(user=self.request.user)
		context['counter'] = context['orders'].count()
		return context

class BasketView(LoginRequiredMixin, ListView):
	'''Represent items in active order'''
	login_url = reverse_lazy('authapp:login')
	model = BasketItem

	def get_queryset(self):
		queryset = BasketItem.objects.filter(order_id=self.kwargs['pk'])
		return queryset

	def get_context_data(self, **kwargs):
		context = super(BasketView, self).get_context_data(**kwargs)
		items = self.get_queryset()
		total_sum = sum([int(item.product.price) * item.quantity for item in items])
		total_quantity = sum([item.quantity for item in items])
		headlines = ['Product', 'Image', 'Quantity', 'Price']
		context.update({'total_sum': total_sum, 'total_quantity': total_quantity, 'headlines': headlines})
		return context


class DeleteBasketView(LoginRequiredMixin, DeleteView):
	'''Class that delete order'''
	login_url = reverse_lazy('authapp:login')
	model = Basket
	success_url = reverse_lazy('shopapp:catalogue')


class RemoveFromBasketView(LoginRequiredMixin, DeleteView):
	'''Class that delete item from order'''
	login_url = reverse_lazy('authapp:login')
	model = BasketItem

	def get_success_url(self):
		order = Basket.objects.get(user=self.request.user, status=True)
		if BasketItem.objects.filter(order_id=order).count() == 1:
			order.delete()
			return reverse_lazy('shopapp:main')
		else:
			return reverse_lazy('basketapp:basket', kwargs={'pk':order.id})
		