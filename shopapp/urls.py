from django.urls import path
from shopapp.views import MainListView, CatalogueListView, ContactsListView, AuthView, ProductCreateView, ProductDeleteView, \
 ChangeProductView, UpdateProductImageView, ProductView
import os
# OrderRedirectView, CreateOrderView


app_name = 'shopapp'

urlpatterns = [
    path('', MainListView.as_view(), name='main'),
    path('catalogue/', CatalogueListView.as_view(), name='catalogue'),
    path('product/<int:pk>/', ProductView.as_view(), name='product'),
    path('catalogue/category/<int:pk>/', CatalogueListView.as_view(), name='category'),
    path('contacts/', ContactsListView.as_view(), name='contacts'),
    path('product/update/<int:pk>/', ChangeProductView.as_view(), name='update'),
    path('update_image/product/<int:pk>/<slug:slug>/', UpdateProductImageView.as_view(), name='image'),
    path('add_product/', ProductCreateView.as_view(), name='add'),
    path('index/', MainListView.as_view(), name='main'),
    path('auth/', AuthView.as_view(), name='auth'),
    path('product/<int:pk>/delete/', ProductDeleteView.as_view(), name='delete')
    ]
