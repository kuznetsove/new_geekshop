from django.contrib import admin
from shopapp.models import ProductCategory, Product, Manufacturer
# Register your models here.

class ProductCategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'description', 'active', 'sort')

admin.site.register(ProductCategory, ProductCategoryAdmin)

class ProductAdmin(admin.ModelAdmin):
	list_display = ('name', 'category', 'description', 'image', 'big_image', 'active', 'sort')

admin.site.register(Product, ProductAdmin)

class ManufacturerAdmin(admin.ModelAdmin):
	list_display =  ('name', 'active')

admin.site.register(Manufacturer, ManufacturerAdmin)
