from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from shopapp.models import Product, ProductCategory
from basketapp.models import Basket, BasketItem
# from django.forms import Form, FileInput
# from django import forms
from shopapp.forms import AddProductForm
# from django.views import View
# from django.views.generic.detail import SingleObjectMixin
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, ModelFormMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


class MainListView(ListView):
	"""Class that represent loading of main page with two random images from Product model"""
	model = Product
	template_name = 'shopapp/index.html'

	def get_context_data(self, **kwargs):
		context = super(MainListView, self).get_context_data(**kwargs)
		context['prod_a'], context['prod_b'] = self.get_queryset().order_by('?')[0:2] # ошибка если меньше двух товаров!!!
		return context


class CatalogueListView(ListView):
	"""Class that represent catalogue page and load data from Produc model"""
	model = Product
	paginate_by = 6
	paginate_orphans = 0
	product_columns = 3
	# включи чтобы убрать предупреждение
	ordering = ['id']

	def get_queryset(self):
		return super().get_queryset().filter(category_id=self.kwargs.get('pk')) if 'pk' in self.kwargs else super().get_queryset()

	def get_yelded_paginator(self, objects_for_pagination):
		paginator = self.paginator_class(objects_for_pagination, self.product_columns, self.paginate_orphans)
		for page_num in paginator.page_range:
			yield paginator.get_page(page_num)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(categories=ProductCategory.objects.all(), **kwargs)
		context['obj_pages'] = self.get_yelded_paginator(context['page_obj'] if context['is_paginated'] else context['object_list'])
		return context


class ChangeProductView(LoginRequiredMixin, UpdateView):
	"""Class that represent updating data for product"""
	model = Product
	login_url = reverse_lazy('authapp:login')
	fields = ['brand', 'name', 'category', 'price', 'quantity']
	success_url = reverse_lazy('shopapp:catalogue')


class UpdateProductImageView(LoginRequiredMixin, UpdateView):
	'''Class that represent updating image for product'''
	model = Product
	login_url = reverse_lazy('authapp:login')
	template_name = 'shopapp/update_image.html'
	success_url = reverse_lazy('shopapp:catalogue')

	def get_context_data(self, **kwargs):
		slug = self.kwargs['slug']
		self.fields = (slug, )
		context = super(UpdateProductImageView, self).get_context_data(**kwargs)
		context['image'] = getattr(self.object, slug)
		context['widget'] = context['form'].fields[slug].widget.render(slug, None)
		return context

class ProductView(LoginRequiredMixin, CreateView):
	'''Class that represent detail information about distinct product with 'add to basket' functionality'''
	model = Product
	login_url = reverse_lazy('authapp:login')
	template_name = 'shopapp/product_detail.html'
	form_class = AddProductForm

	def get_object(self):
		pk = self.kwargs['pk']
		obj = Product.objects.get(id=pk)
		return obj

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		obj = self.get_object()
		obj_id = getattr(obj, 'id', 0)
		cat_id = getattr(obj, 'category', 0)
		similar = Product.objects.exclude(id=obj_id).filter(category=cat_id)[:3]
		context.update({'object': obj, 'similar': similar})
		return context

	def form_valid(self, form):
		product = self.get_object()
		if Basket.objects.get_active(self.request.user).exists() == False:
			print(Basket.objects)
			new_order = Basket(user=self.request.user)
			new_order.save()
			return HttpResponseRedirect(self.get_success_url())
		elif Basket.objects.get_active(self.request.user):
			order_id = Basket.objects.get_order(self.request.user)
			if self.kwargs['pk'] in BasketItem.objects.orders_list(order_id):
				obj = BasketItem.objects.get(order_id=order_id, product=product)
				obj.quantity += int(self.request.POST.get('quantity'))
				obj.save()
				return HttpResponseRedirect(self.get_success_url())
			else:
				self.object = form.save(commit=False)
				self.object.product = product
				self.object.order_id = order_id
				self.object.quantity = int(self.request.POST.get('quantity'))
				self.object.save()
				return super(ModelFormMixin, self).form_valid(form)

	def get_success_url(self):
		order_id = Basket.objects.get_order(self.request.user)
		if self.kwargs['pk'] not in BasketItem.objects.orders_list(order_id):
			obj = BasketItem(order_id=order_id)
			obj.product = self.get_object()
			obj.quantity = int(self.request.POST.get('quantity'))
			obj.save()
			return reverse_lazy('basketapp:basket', kwargs={'pk':order_id.id})
		else:
			return reverse_lazy('basketapp:basket', kwargs={'pk':order_id.id})


class ContactsListView(TemplateView):
	"""Class that represent contacts page"""
	template_name = 'shopapp/contacts.html'


class AuthView(TemplateView):
	"""Class that represent page for choosing login or register redirect"""
	template_name = 'shopapp/auth.html'


class ProductCreateView(CreateView):
	"""Class that represent page for creating new record in Product model"""
	model = Product
	template_name_suffix = '_create_form'
	# template_name = 'shopapp/create_product_form.html'
	fields = ['brand', 'name', 'category', 'image', 'big_image', 'price', 'quantity']


class ProductDeleteView(LoginRequiredMixin, DeleteView):
	"""Class that represent page for deleting record from product model"""
	model = Product
	login_url = reverse_lazy('authapp:login')
	success_url = reverse_lazy('shopapp:catalogue')
