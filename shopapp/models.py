from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Core(models.Model):
	sort = models.IntegerField('sort', default=0, null=True)
	active = models.BooleanField('active', default=False)

class ProductCategory(Core):

    class Meta:
        verbose_name = _('Product category')
        verbose_name_plural = _('Product categories')

    name = models.CharField(verbose_name=_('Category name'), max_length=64, unique=True)
    description = models.TextField(verbose_name=_('Category description'), blank=True)

    def __str__(self):
        return self.name

class Manufacturer(Core):
    """Класс отображающий таблицу 'Производитель' в базе данных"""
    class Meta:
        verbose_name = _('Manufacturer')
        verbose_name_plural = _('Manufacturers')

    name = models.CharField(verbose_name=_('Manufacturer name'), max_length=128)

    def __str__(self):
        return self.name

class Product(Core):

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_('product name'), max_length=128)
    image = models.ImageField(upload_to='products_images', verbose_name=_('Small product picture'), blank=True, help_text= '')
    big_image = models.ImageField(upload_to='products_images', verbose_name=_('Big product picture'), blank=True, help_text= '')
    description = models.TextField(verbose_name=_('product description'), blank=True)
    price = models.DecimalField(verbose_name='price', max_digits=8, decimal_places=2, default=0)
    quantity = models.PositiveIntegerField(verbose_name=_('quantity on stock'), default=0)
    brand = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)

    def __str__(self):
        return "{} ({})".format(self.name, self.category.name)

    def get_absolute_url(self):
        return reverse('shopapp:catalogue')



