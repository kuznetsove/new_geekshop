from django.forms import ModelForm, Select, ChoiceField
from basketapp.models import BasketItem

class AddProductForm(ModelForm):
	class Meta:
		model = BasketItem
		fields = ['quantity']

	def __init__(self, *args, **kwargs):
	        super().__init__(*args, **kwargs)
	        self.fields['quantity'].widget.attrs.update({'class':'quantity'})