from django.urls import path
from authapp.views import AuthLoginView, AuthLogoutView, UpdateProfileView, DeleteProfileView, \
	CreateProfileView
app_name = 'authapp'

urlpatterns = [
	path('login/', AuthLoginView.as_view(), name='login'),
	path('logout/', AuthLogoutView.as_view(), name='logout'),
	path('updateuser/', UpdateProfileView.as_view(), name='update'),
	path('deleteuser/<int:pk>', DeleteProfileView.as_view(), name='delete'),
	path('registration/', CreateProfileView.as_view(), name='registration'),
]