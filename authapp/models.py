from django.db import models
from django.contrib.auth.models import AbstractUser
from django import forms
from django.utils.translation import ugettext_lazy as _


class ShopUser(AbstractUser):
    avatar = models.ImageField(upload_to='users_avatars', blank=True)
    age = models.PositiveIntegerField(verbose_name=_('age'), null=True)

    def get_absolute_url(self):
        return reverse('shopapp:main')