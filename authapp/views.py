from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from authapp.forms import ShopUserLoginForm, ShopUserRegisterForm, ShopUserEditForm
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from authapp.models import ShopUser
from django.contrib.auth import authenticate, login
from django.urls import reverse, reverse_lazy
# Create your views here.

class AuthLoginView(LoginView):
	'''Class for login user'''
	success_message = 'You are logged in'
	authappentication_form = ShopUserLoginForm

class AuthLogoutView(LoginRequiredMixin, LogoutView):
	'''Class for logout user'''
	success_message = 'You are logged out'
	login_url = reverse_lazy('authapp:login')

class CreateProfileView(CreateView):
	'''Class for creating user'''
	model = ShopUser
	form = ShopUserRegisterForm
	fields = ('username', 'password', 'email', 'first_name', 'last_name', 'avatar')

	def get_success_url(self):
		self.object.set_password(self.object.password)
		self.object.save()
		return reverse_lazy('authapp:login')

class UpdateProfileView(LoginRequiredMixin, UpdateView):
	'''Class for updating user profile'''
	form_class = ShopUserEditForm
	template_name = 'authapp/change_profile.html'
	login_url = reverse_lazy('authapp:login')

	def get_object(self, queryset=None):
		return self.request.user

	def get_success_url(self):
		return reverse_lazy('authapp:update')


class DeleteProfileView(LoginRequiredMixin, DeleteView):
	'''Class for deleting user'''
	model = ShopUser
	success_url = reverse_lazy('shopapp:main')
	login_url = reverse_lazy('authapp:login')

	def get_object(self, queryset=None):
		return self.request.user