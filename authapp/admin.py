from django.contrib import admin
from authapp.models import ShopUser

# Register your models here.
class ShopUserAdmin(admin.ModelAdmin):
	list_display = ['username', 'first_name', 'last_name', 'email', 'age']

admin.site.register(ShopUser, ShopUserAdmin)


